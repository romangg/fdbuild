# SPDX-FileCopyrightText: 2022 Francesco Sorrentino <francesco.sorr@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# A basic fdbuild step and template paths getter
class Plugin:
    def __init__(self, step):
        pass

    def work(self):
        print("Hello, world!")
