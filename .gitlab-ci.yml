# SPDX-FileCopyrightText: 2022 Roman Gilg <subdiff@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

stages:
  - Compliance
  - Analysis
  - Template Builds

workflow:
  rules:
    - when: always

include:
  - project: kwinft/tooling
    ref: master
    file:
      - '/docs/gitlab-ci/commits.yml'


####################################################################################################
#
# Compliance
#

Message Lint:
  extends: .message-lint
  stage: Compliance

Copyright Lint:
  stage: Compliance
  image: python
  before_script:
    - pip install tox
  script:
    - tox -e reuse

Code Style:
  stage: Compliance
  image: python
  before_script:
    - pip install tox
  script:
    - tox -e black

Templates Style:
  stage: Compliance
  image: python
  before_script:
    - pip install tox
  script:
    - tox -e yamllint

####################################################################################################
#
# Analysis
#

.common-tests:
  stage: Analysis
  before_script:
    - pip install tox
  script:
    - tox -e sample-plugin,templates

Old Python Tests:
  extends: .common-tests
  image: python:3.8

Tests:
  extends: .common-tests
  image: python


####################################################################################################
#
# Template Builds
#

.template-builds-common:
  stage: Template Builds
  when: manual
  image: archlinux
  before_script:
    - pacman -Syu --quiet --noconfirm
    - pacman -S --needed --quiet --noconfirm python-pip git
    - python -m venv /opt/venv
    - source /opt/venv/bin/activate
    - pip install .

.template-builds-graphics:
  extends: .template-builds-common
  before_script:
    - !reference [.template-builds-common, before_script]
    - pip install mako
    - pacman -S --needed --quiet --noconfirm
        base-devel clang cmake llvm meson ninja pkgconf
        check glslang gperf libevdev libfontenc libva
        libva-vdpau-driver libwacom libxslt mtdev xmlto
        docbook-xsl doxygen graphviz

linux-graphics-meta:
  extends: .template-builds-graphics
  script:
    - fdbuild --init-with-template linux-graphics-meta
    # Install to /usr so pkgconfig finds libs without hiccups.
    - "sed -i 's.  path: /usr/local.  path: /usr.g' linux-graphics-meta/fdbuild.yaml"
    - fdbuild -w linux-graphics-meta

linux-graphics-meta-qt5:
  extends: .template-builds-graphics
  before_script:
    - !reference [.template-builds-graphics, before_script]
    - pacman -S --needed --quiet --noconfirm
        perl-uri pcre
  script:
    - fdbuild --init-with-template linux-desktop-meta
    # Install to /usr so pkgconfig finds libs without hiccups.
    - "sed -i 's.  path: /usr/local.  path: /usr.g' linux-desktop-meta/fdbuild.yaml"
    - fdbuild -w linux-desktop-meta/linux-graphics-meta
    - fdbuild -w linux-desktop-meta/qt5

linux-desktop-meta:
  extends: .template-builds-graphics
  before_script:
    - !reference [linux-graphics-meta-qt5, before_script]
    - pip install pycairo
    - pacman -S --needed --quiet --noconfirm
        boost exiv2 gobject-introspection gsettings-desktop-schemas gtk-doc itstool
        libcanberra libnm libpulse libstemmer libqalculate libyaml lmdb
        modemmanager pipewire polkit sassc qrencode zxing-cpp
  script:
    - fdbuild --init-with-template linux-desktop-meta
    # Install to /usr so pkgconfig finds libs without hiccups.
    - "sed -i 's.  path: /usr/local.  path: /usr.g' linux-desktop-meta/fdbuild.yaml"
    # Enable Release builds to speed up.
    - "sed -i '/- -developer-build/d' linux-desktop-meta/qt5/fdbuild.yaml"
    - 'printf "configure:\n  options:\n    - CMAKE_BUILD_TYPE=Release" >>
       linux-desktop-meta/kwinft-plasma-meta/kde/fdbuild.yaml'
    # breeze-grub comes without CMake file
    # TODO(romangg): need to adapt structurizer, so it adds a hook instead
    - "sed -i '/- breeze-grub/d' linux-desktop-meta/kwinft-plasma-meta/kde/fdbuild.yaml"
    # plymouth is in AUR
    - "sed -i '/- breeze-plymouth/d' linux-desktop-meta/kwinft-plasma-meta/kde/fdbuild.yaml"
    - "sed -i '/- plymouth-kcm/d' linux-desktop-meta/kwinft-plasma-meta/kde/fdbuild.yaml"
    # Pulls in all of GTK3.
    - "sed -i '/- kde-gtk-config/d' linux-desktop-meta/kwinft-plasma-meta/kde/fdbuild.yaml"
    # Pulls in ffmpeg, which again pulls in X libraries.
    - "sed -i '/- kpipewire/d' linux-desktop-meta/kwinft-plasma-meta/kde/fdbuild.yaml"
    - fdbuild -w linux-desktop-meta
